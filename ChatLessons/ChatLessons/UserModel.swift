//
//  UserModel.swift
//  ChatLessons
//
//  Created by Michail Bondarenko on 2/18/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation

struct UserModel {
    let name: String = "Max"
    let lastMessage: String = "Last message ...."
    let iconURL: String = "https://png.pngtree.com/svg/20170921/the_default_user__1213197.png"
}

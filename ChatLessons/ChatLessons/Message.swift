//
//  Message.swift
//  ChatLessons
//
//  Created by Michail Bondarenko on 3/4/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation

struct Message {
    
    let id: String = ""
    let text: String
    let sender: String = ""
    let receiver: String = ""
    let timestamp: Double
    
    
}
